get "/s/@guid", forward: "/WEB-INF/groovy/s/survey.groovy?guid=@guid", cache : 10.minutes

get "/", forward: "/WEB-INF/pages/index.gtpl"
get "/datetime", forward: "/datetime.groovy"

get "/favicon.ico", redirect: "/images/gaelyk-small-favicon.png"
