/**
 * Twitsurvey.me project. Copyright (c) 2011 Ondrej Linek (ondrej@linek.co.uk)
 * Use and reuse only upon written permission.
 * User: def
 * Date: 11/22/11
 * Time: 8:21 PM
 */
package me.twitsurvey.domain

import javax.persistence.Id

class Survey implements Serializable {
    @Id String guid
    // foreign keys
    String theme = "default" // any css file in "css" - css appended automatically
    String billingInfo = "free" // free/billing information link
    String owner

    // members
    String name
    String description
    Boolean enabled // can be disabled
    Date validFrom // can be null meaning it's valid from always
    Date validTo // can be null meaning it's never expires
    Date deleteBy // deleteable after this time elapses
    String accessLevel = "public" // public/url/password
    String password
    Boolean openForAll // everyone can participate
    List<String> participants // people who can participate if openForAll == false
    Integer maxHits // how many times this survey can be displayed
    Integer hits // current number of hits (there can be a separate entity for this one
    Integer maxTakenNo // how many times this survey can be taken
    String closedNote // html to display if survey can't be taken (for whatever reason)
    String thankYouNote // html to display if survey completed
    
    //questions
    List<String> questions
    
}