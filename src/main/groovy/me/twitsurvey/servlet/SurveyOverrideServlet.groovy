package me.twitsurvey.servlet

import com.googlecode.objectify.ObjectifyService
import groovy.servlet.ServletBinding
import groovyx.gaelyk.GaelykServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import static me.twitsurvey.util.Bootstrap.bootstrap

/**
 * Twitsurvey.me project. Copyright (c) 2011 Ondrej Linek (ondrej@linek.co.uk)
 * Use and reuse only upon written permission.
 * User: def
 * Date: 12/1/11
 * Time: 8:00 AM
 */
class SurveyOverrideServlet extends GaelykServlet {
    @Override
    void init(javax.servlet.ServletConfig config) {
        super.init(config)
        bootstrap()
    }

    @Override
    protected void setVariables(ServletBinding binding) {
        super.setVariables(binding)
        binding.setVariable ("objectify", null)
    }

    @Override
    void service(HttpServletRequest request, HttpServletResponse response) {
        // inject objectify service
        objectify = ObjectifyService.beginTransaction()
        super.service(request, response)
        if (objectify.txn.active) {
            objectify.txn.commit()
        }
        // commit all stuff
    }


}
