/**
 * Twitsurvey.me project. Copyright (c) 2011 Ondrej Linek (ondrej@linek.co.uk)
 * Use and reuse only upon written permission.
 * User: def
 * Date: 11/25/11
 * Time: 12:32 PM
 */
package me.twitsurvey.service

import me.twitsurvey.domain.Survey
import com.googlecode.objectify.ObjectifyService
import com.googlecode.objectify.Objectify


class SurveyService {

    Survey getSurvey (def request, String guid) {
        Objectify objectify = ObjectifyService.begin()
        Survey survey = objectify.get(Survey.class, guid)
        isSurveable (request, survey) ? survey : null
    }
    
    Boolean isSurveable (def request, Survey survey) {
        return true
    }
}
