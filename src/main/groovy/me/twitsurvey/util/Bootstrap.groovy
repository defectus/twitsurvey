package me.twitsurvey.util

import me.twitsurvey.domain.Survey
import static com.googlecode.objectify.ObjectifyService.register

/**
 * Twitsurvey.me project. Copyright (c) 2011 Ondrej Linek (ondrej@linek.co.uk)
 * Use and reuse only upon written permission.
 * User: def
 * Date: 12/12/11
 * Time: 5:50 PM
 */

class Bootstrap {
    static bootstrap() {
        register Survey
        // register other domain classes preferably by traversing the domain folder
    }
}